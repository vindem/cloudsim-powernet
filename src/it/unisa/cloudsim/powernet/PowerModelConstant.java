package it.unisa.cloudsim.powernet;

import org.cloudbus.cloudsim.power.models.PowerModel;

public class PowerModelConstant implements PowerModel {

	private double powerConstant;
	
	public PowerModelConstant(double powerConstant){
		this.powerConstant = powerConstant;
	}
	
	@Override
	public double getPower(double utilization) throws IllegalArgumentException {
		return powerConstant;
	}

}
