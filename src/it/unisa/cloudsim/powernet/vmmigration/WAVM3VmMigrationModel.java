package it.unisa.cloudsim.powernet.vmmigration;

import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;

import it.unisa.cloudsim.NAConsolidationConstants;
import it.unisa.cloudsim.powernet.PowerNetHost;

public class WAVM3VMMigrationModel extends VMMigrationModel {
	
	final int SOURCE = 0;
	final int TARGET = 1;
	
	double initiationTimeInSeconds = 5.0;
	double activationTimeInSeconds = 5.0;
	double dirtyingRate = NAConsolidationConstants.PAGE_DIRTYING_RATE;
	double pageSize = NAConsolidationConstants.PAGE_SIZE;
	
	//Initiation phase coefficients
	private final double[] alphaI = { 1.71, 3.18 };
	private final double[] betaI = {1.41, 0.0};
	private final double[] CI = {0.0, 0.0};
	//final double CI_src = 708.3;
	//final double CI_trg = 596.06;
	
	//Transfer phase coefficients
	private final double[] alphaT = { 2.4, 2.56 };
	private final double[] betaT = { 1.52e-6, 7.32e-7 };
	private final double[] gammaT = {1.41,0};
	private final double[] deltaT = {0.4,0.4};
	private final double[] CT = {24.67, 24.67};
	//calculated as the median between (520.214 - idle(opteron)) and (520.214 - idle(xeon)) 
	//private final double[] CT = {190.307, 190.307};
	//private final double[] CT = {421.74, 520.214};
		
	//Activation phase coefficients
	private final double[] alphaA = {2.37, 1.88};
	private final double[] betaA = {0, 17.01};
	private final double[] CA = {0, 0};
	//private final double[] CA = {662.5, 499.5};
	
	@Override
	public double getEnergyConsumption(Vm vm, PowerHost source,PowerHost target, boolean isSource) {
		double energy = 0.0;
		PowerNetHost sourceHost = (PowerNetHost) source;
		PowerNetHost targetHost = (PowerNetHost) target;
								
		energy += (isSource)? 
				computeInitiationEnergyOfSource(vm,sourceHost,targetHost) :
					computeInitiationEnergyOfTarget(vm,sourceHost,targetHost);
		energy += (isSource)?
				computeTransferEnergyOfSource(vm,sourceHost,targetHost) :
					computeTransferEnergyOfTarget(vm,sourceHost,targetHost)	;
		energy += (isSource)?
				computeActivationEnergyOfSource(vm,sourceHost,targetHost):
					computeActivationEnergyOfTarget(vm,sourceHost,targetHost)	;
		
		return energy;
	}

	private double computeActivationEnergyOfSource(Vm vm, PowerHost sourceHost, PowerHost targetHost) {
		double srcActivPower = alphaA[SOURCE] * sourceHost.getUtilizationOfCpu()
				+ betaA[SOURCE] * vm.getTotalUtilizationOfCpu(1.0) + CA[SOURCE];
		
		return srcActivPower * activationTimeInSeconds;
	}
	
	private double computeActivationEnergyOfTarget(Vm vm, PowerHost sourceHost, PowerHost targetHost) {
		double trgActivPower = alphaA[TARGET] * targetHost.getUtilizationOfCpu()
				+ betaA[TARGET] * vm.getTotalUtilizationOfCpu(1.0) + CA[TARGET];
		return trgActivPower * activationTimeInSeconds;
	}

	private double computeTransferEnergyOfSource(Vm vm, PowerNetHost sourceHost, PowerNetHost targetHost) {
		double srcTransfEnergy;
				
				
		double numHops = sourceHost.getHopNumber(targetHost);
				
		srcTransfEnergy = alphaT[SOURCE] * sourceHost.getUtilizationOfCpu() + betaT[SOURCE] * sourceHost.getBw() +
				gammaT[SOURCE] * dirtyingRate + deltaT[SOURCE] * vm.getTotalUtilizationOfCpu(1.0) + CT[SOURCE];
		return (srcTransfEnergy) * getTransferTimeForSingleHop(vm.getRam(), sourceHost.getBw(), targetHost.getBw()) * numHops;
	}

	private double computeTransferEnergyOfTarget(Vm vm, PowerNetHost sourceHost, PowerNetHost targetHost) {
		double trgTransfEnergy;
		
		double numHops = sourceHost.getHopNumber(targetHost);
				
		trgTransfEnergy = alphaT[TARGET] * targetHost.getUtilizationOfCpu() + betaT[TARGET] * targetHost.getBw() +
				gammaT[TARGET] * dirtyingRate + deltaT[TARGET] * vm.getTotalUtilizationOfCpu(1.0) + CT[TARGET];
		return (trgTransfEnergy) * getTransferTimeForSingleHop(vm.getRam(), sourceHost.getBw(), targetHost.getBw()) * numHops;
	}
	
	private double computeInitiationEnergyOfSource(Vm vm, PowerHost sourceHost, PowerHost targetHost)
	{
		double srcInitPower = alphaI[SOURCE] * sourceHost.getUtilizationOfCpu()
				+ betaI[SOURCE] * vm.getTotalUtilizationOfCpu(1.0) + CI[SOURCE];
		return srcInitPower * initiationTimeInSeconds;
	}
	
	private double computeInitiationEnergyOfTarget(Vm vm, PowerHost sourceHost, PowerHost targetHost)
	{
		double trgInitPower = alphaI[TARGET] * targetHost.getUtilizationOfCpu()
				+ betaI[TARGET] * vm.getTotalUtilizationOfCpu(1.0) + CI[TARGET];
		return trgInitPower * initiationTimeInSeconds;
	}

	@Override
	public double getEnergyConsumptionOnSource(Vm vm, PowerHost source, PowerHost target) {
		return getEnergyConsumption(vm, source, target, true);
	}

	@Override
	public double getEnergyConsumptionOnTarget(Vm vm, PowerHost source, PowerHost target) {
		return getEnergyConsumption(vm, source, target, false);
	}
	
	public double getTransferTimeForSingleHop(double vmRam, double sourceBw, double targetBw){
		double tTime = (vmRam/(sourceBw*0.125)) + 
				(((vmRam/NAConsolidationConstants.PAGE_SIZE)
						*NAConsolidationConstants.PAGE_DIRTYING_RATE)/(targetBw*0.125))
				*NAConsolidationConstants.VM_MIGRATION_PRECOPY_ITERATION ;
		return tTime;
	}
}
