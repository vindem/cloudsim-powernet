package it.unisa.cloudsim.powernet.vmmigration;

import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;

public class LiuVMMigrationModel extends VMMigrationModel {

	/* These constants come from the paper
	 * 	"Performance and Energy Modeling for Live Migration of Virtual Machines"
		Haikun Liu, Cheng-Zhong Xu, Hai Jin, Jiayu Gong, Xiaofei Liao
	 */
	private final double alpha = 0.512;
	private final double beta = 20.152;
	
	@Override
	public double getEnergyConsumption(Vm vm, PowerHost source, PowerHost target, boolean isSource) {
		return (alpha * vm.getRam() + beta) * 2.0;
	}

	@Override
	public double getEnergyConsumptionOnSource(Vm vm, PowerHost source, PowerHost target) {
		return (alpha * vm.getRam() + beta);
	}

	@Override
	public double getEnergyConsumptionOnTarget(Vm vm, PowerHost source, PowerHost target) {
		return (alpha * vm.getRam() + beta);
	}

}
