package it.unisa.cloudsim.powernet.vmmigration;

import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;

public abstract class VMMigrationModel {

	public abstract double getEnergyConsumption(Vm vm, PowerHost source, PowerHost target, boolean isSource);

	public abstract double getEnergyConsumptionOnSource(Vm vm, PowerHost source, PowerHost target);
	
	public abstract double getEnergyConsumptionOnTarget(Vm vm, PowerHost source, PowerHost target);

	public double getTransferTimeForSingleHop(double vmRam, double sourceBw, double targetBw) {
		double tTime = vmRam / (Math.min(sourceBw, targetBw)*0.125);
		return tTime;
	}
	
}
