package it.unisa.cloudsim.powernet.vmmigration.policies;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerVmAllocationPolicyMigrationAbstract;
import org.cloudbus.cloudsim.power.PowerVmSelectionPolicy;
import org.cloudbus.cloudsim.power.lists.PowerVmList;

import it.unisa.cloudsim.powernet.PowerNetHost;

public class ClosestTargetVMAllocationPolicy extends PowerVmAllocationPolicyMigrationAbstract {

	public ClosestTargetVMAllocationPolicy(List<? extends Host> hostList, PowerVmSelectionPolicy vmSelectionPolicy) {
		super(hostList, vmSelectionPolicy);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected boolean isHostOverUtilized(PowerHost host) {
		// TODO Auto-generated method stub
		return false;
	}
	
	protected List<Map<String, Object>> getNewVmPlacement(
			List<? extends Vm> vmsToMigrate,
			Set<? extends Host> excludedHosts) {
		List<Map<String, Object>> migrationMap = new LinkedList<Map<String, Object>>();
		PowerVmList.sortByCpuUtilization(vmsToMigrate);
		for (Vm vm : vmsToMigrate) {
			PowerHost allocatedHost = findHostForVm(vm, excludedHosts);
			if (allocatedHost != null) {
				PowerHost prevHost = (PowerHost) vm.getPreviousHost();
				allocatedHost.vmCreate(vm);
				Log.printConcatLine("VM #", vm.getId(), " allocated to host #", allocatedHost.getId());

				Map<String, Object> migrate = new HashMap<String, Object>();
				migrate.put("vm", vm);
				migrate.put("previousHost", prevHost);
				migrate.put("host", allocatedHost);
				migrationMap.add(migrate);
			}
		}
		return migrationMap;
	}

	public PowerNetHost findHostForVm(Vm vm, Set<? extends Host> excludedHosts) {
		double minPower = Double.MAX_VALUE;
		double minHops = Double.MAX_VALUE;
		PowerNetHost allocatedHost = null;
		
		for (PowerNetHost host : this.<PowerNetHost> getHostList()) {
			if (excludedHosts.contains(host)) {
				continue;
			}
			if (host.isSuitableForVm(vm)) {
				if (getUtilizationOfCpuMips(host) != 0 && isHostOverUtilizedAfterAllocation(host, vm)) {
					continue;
				}
				try {
					double currHops = host.getHopNumber(((PowerNetHost)vm.getPreviousHost()));
					if(currHops < minHops){
						minHops = currHops;
						double powerAfterAllocation = getPowerAfterAllocation(host, vm);
						if (powerAfterAllocation != -1) {
							double powerDiff = powerAfterAllocation - host.getPower();
							if (powerDiff < minPower) {
								minPower = powerDiff;
								allocatedHost = host;
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
				}
		}
				
		return allocatedHost;
	}
	
}
