package it.unisa.cloudsim.powernet.vmmigration;

import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;

public class ConstantVMMigrationModel extends VMMigrationModel {

	final double constant = 0;
	
	@Override
	public double getEnergyConsumption(Vm vm, PowerHost source, PowerHost target, boolean isSource) {
		return constant;
	}

	public double getEnergyConsumptionOnSource(Vm vm, PowerHost source, PowerHost target) {
		return constant;
	}
	
	public double getEnergyConsumptionOnTarget(Vm vm, PowerHost source, PowerHost target) {
		return constant;
	}
	
}
