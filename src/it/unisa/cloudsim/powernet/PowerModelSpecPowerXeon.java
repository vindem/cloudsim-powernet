package it.unisa.cloudsim.powernet;

import org.cloudbus.cloudsim.power.models.PowerModelSpecPower;

public class PowerModelSpecPowerXeon extends PowerModelSpecPower {

	private final double[] power = { 164.2693155, 258.4939556, 283.5509239, 297.5280146, 299.7377091,
			310.1294975, 320.5212859, 330.9130743, 349.098704, 353.0827854, 368.0950213};
	
	@Override
	protected double getPowerData(int index) {
		// TODO Auto-generated method stub
		return power[index];
	}

}
