package it.unisa.cloudsim.powernet;

import java.util.ArrayList;
import java.util.List;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Pe;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.VmScheduler;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.models.PowerModel;
import org.cloudbus.cloudsim.provisioners.BwProvisioner;
import org.cloudbus.cloudsim.provisioners.RamProvisioner;

import it.unisa.cloudsim.NAConsolidationConstants;
import it.unisa.cloudsim.overcommitment.PowerMeasurable;

public class DataCenterSwitch extends Host implements PowerMeasurable  {

	//the amount of hosts the switch is able to connect
	private final int capacity = NAConsolidationConstants.SWITCH_CAPACITY;
	private PowerModel powerModel;
	int id;
	BwProvisioner bwProvisioner;
	/**
	 * 
	 * @param id
	 * @param ramProvisioner
	 * @param bwProvisioner
	 */
	
	public DataCenterSwitch(int id, BwProvisioner bwProvisioner){
		super(id, bwProvisioner);
		this.powerModel = (PowerModel)new PowerModelConstant(NAConsolidationConstants.SWITCH_POWER);
		this.id = id;
		this.bwProvisioner = bwProvisioner;
	}
	
	public DataCenterSwitch(int id, BwProvisioner bwProvisioner, PowerModel powerModel){
		super(id, bwProvisioner);
		this.powerModel = powerModel;
		this.id = id;
		this.bwProvisioner = bwProvisioner;
	}
	
	public long getAvailableBw(){
		return this.bwProvisioner.getAvailableBw();
	}
	
	public int getCapacity() {
		return capacity;
	}

	@Override
	public double getPower() {
		return this.powerModel.getPower(getAvailableBw()
				/((double)NAConsolidationConstants.SWITCH_BW));
	}
	
		
}
