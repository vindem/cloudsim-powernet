package it.unisa.cloudsim.powernet.topologies;

import java.util.ArrayList;
import java.util.List;

import org.cloudbus.cloudsim.power.PowerHost;

import it.unisa.cloudsim.powernet.PowerNetHost;

public interface DCNTopology {

	public void setupTopology(List<PowerNetHost> hostList);
	
}
