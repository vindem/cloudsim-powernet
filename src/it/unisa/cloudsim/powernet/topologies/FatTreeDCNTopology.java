package it.unisa.cloudsim.powernet.topologies;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import org.apache.commons.math3.analysis.function.Power;
import org.cloudbus.cloudsim.examples.power.Constants;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.provisioners.BwProvisioner;
import org.cloudbus.cloudsim.provisioners.BwProvisionerSimple;
import org.jgraph.JGraph;
import org.jgrapht.ext.JGraphModelAdapter;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;

import it.unisa.cloudsim.NAConsolidationConstants;

import it.unisa.cloudsim.overcommitment.PowerMeasurable;
import it.unisa.cloudsim.powernet.DataCenterSwitch;
import it.unisa.cloudsim.powernet.PowerModelConstant;
import it.unisa.cloudsim.powernet.PowerNetHost;

public class FatTreeDCNTopology extends SimpleGraph<PowerMeasurable, DefaultEdge>
		implements DCNTopology{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int switchCapacity;
	
	private ArrayList<PowerNetHost> coreLayerSwitches
								= new ArrayList<PowerNetHost>();
	private ArrayList<PowerNetHost> aggregationLayerSwitches 
								= new ArrayList<PowerNetHost>();
	private ArrayList<PowerNetHost> edgeLayerSwitches
								= new ArrayList<PowerNetHost>();
	private List<PowerNetHost> hostList;
	
	final int CORE_PREFIX = 100;
	final int AGGREGATION_PREFIX = 200;
	final int EDGE_PREFIX = 300;

	private int podNum;

	private int aggrEdgeNum;

	private int hostsPerPod;

	private int switchId;
	
	
		
	public FatTreeDCNTopology(Class<? extends DefaultEdge> edgeClass, int switchCapacity) 
	{
		super(edgeClass);
		this.switchCapacity = switchCapacity;
	}

	@Override
	public void setupTopology(List<PowerNetHost> hostList)
	{
		this.hostList = hostList;
		this.hostsPerPod = (int) Math.pow(NAConsolidationConstants.SWITCH_CAPACITY/2.0,2);
		double tmp = (Math.round(hostList.size() / hostsPerPod +0.5 ));
		this.podNum = (int) tmp;
		this.aggrEdgeNum = 2 * podNum * podNum;
		setupHosts();
		setupEdgeLayer(aggrEdgeNum);
		setupAggregationLayer(aggrEdgeNum);
		setupCoreLayer(podNum);
		createCoreToAggregationLayerLinks();
		createAggregationToEdgeLayerLinks();
		createEdgeToHostsLinks();
		System.out.println("Setup FAT-TREE TOPOLOGY with " + vertexSet().size()
				+ " vertices, " + edgeSet().size() + " edges.");
	}

	private void createEdgeToHostsLinks() {
		int j = 0;
		for(int i = 1 ; i < edgeLayerSwitches.size(); i++)
		{
			for(; j < hostList.size(); j++)
			{
				{
					if( edgeLayerSwitches.get(i) != hostList.get(j) )
					{
						System.out.println("Add edge between " + edgeLayerSwitches.get(i).getId() + " and " + hostList.get(j).getId());
						addEdge( edgeLayerSwitches.get(i), hostList.get(j));
					}
				}
			}
		}
	}

	private void createAggregationToEdgeLayerLinks() {
		for(int i = 0; i < aggregationLayerSwitches.size(); i++)
			for(int j = 1; j < edgeLayerSwitches.size(); j++)
				addEdge(aggregationLayerSwitches.get(i),edgeLayerSwitches.get(j));
		
	}

	private void createCoreToAggregationLayerLinks() {
		for(int i = 0; i < coreLayerSwitches.size(); i++)
			for(int j = i + aggrEdgeNum; j < aggregationLayerSwitches.size(); j+= aggrEdgeNum)
				addEdge(coreLayerSwitches.get(i),aggregationLayerSwitches.get(j));
	}

	private void setupHosts() {
		for(PowerNetHost pnh : hostList)
			addVertex(pnh);
		switchId = hostList.size();		
	}

	private void setupEdgeLayer(int num) {
		System.out.println("Adding " + num + " edge switches.");
		for(int i = 0; i < num; i++)
		{
			PowerNetHost sw = new PowerNetHost(getID(), 
					new BwProvisionerSimple(NAConsolidationConstants.SWITCH_BW),
					new PowerModelConstant(10.0),
					NAConsolidationConstants.MIGRATION_ENERGY_MODEL);
			addVertex(sw);
			edgeLayerSwitches.add(sw);
			hostList.add(sw);
		}
		
	}

	private int getID() {
		int oldId = switchId;
		switchId++;
		return oldId;
	}

	private void setupAggregationLayer(int num) {
		System.out.println("Adding " + num + " aggregation switches.");
		for(int i = 0; i < num; i++)
		{
			PowerNetHost sw = new PowerNetHost(getID(), 
					new BwProvisionerSimple(NAConsolidationConstants.SWITCH_BW),new PowerModelConstant(10.0),
					NAConsolidationConstants.MIGRATION_ENERGY_MODEL);
			addVertex(sw);
			aggregationLayerSwitches.add(sw);
		}
		
	}

	private void setupCoreLayer(int num) {
		System.out.println("Adding " + num + " core switches.");
		for(int i = 0; i < num; i++)
		{
			PowerNetHost sw = new PowerNetHost(getID(), 
					new BwProvisionerSimple(NAConsolidationConstants.SWITCH_BW),new PowerModelConstant(10.0),
					NAConsolidationConstants.MIGRATION_ENERGY_MODEL);
			addVertex(sw);
			coreLayerSwitches.add(sw);
		}
			
	}
	
			
}
