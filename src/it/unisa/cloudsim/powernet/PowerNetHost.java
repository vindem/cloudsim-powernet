package it.unisa.cloudsim.powernet;

import java.util.List;

import org.cloudbus.cloudsim.Pe;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.VmScheduler;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerHostUtilizationHistory;
import org.cloudbus.cloudsim.power.models.PowerModel;
import org.cloudbus.cloudsim.provisioners.BwProvisioner;
import org.cloudbus.cloudsim.provisioners.RamProvisioner;

import it.unisa.cloudsim.overcommitment.PowerMeasurable;
import it.unisa.cloudsim.overcommitment.PowerNetDataCentre;
import it.unisa.cloudsim.powernet.vmmigration.VMMigrationModel;

public class PowerNetHost extends PowerHostUtilizationHistory implements PowerMeasurable{

	private DataCenterSwitch higherLevelSwitch;
	private Class<? extends VMMigrationModel> migModelClass;
	private VMMigrationModel migModel;
	
	public PowerNetHost(int id, BwProvisioner bwProvisioner, PowerModel model, Class<? extends VMMigrationModel> mIGRATION_ENERGY_MODEL){
		super(id, bwProvisioner, model);
		this.migModelClass = mIGRATION_ENERGY_MODEL;
		try {
			migModel = this.migModelClass.newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public PowerNetHost(int id, RamProvisioner ramProvisioner, BwProvisioner bwProvisioner, long storage,
			List<? extends Pe> peList, VmScheduler vmScheduler, PowerModel powerModel,
			Class<? extends VMMigrationModel> migModelClass) {
		super(id, ramProvisioner, bwProvisioner, storage, peList, vmScheduler, powerModel);
		this.migModelClass = migModelClass;
		try {
			migModel = this.migModelClass.newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// TODO Auto-generated constructor stub
	}

	public void setHigherLevelSwitch(DataCenterSwitch HLSwitch){
		this.higherLevelSwitch = HLSwitch;
	}
	
	public DataCenterSwitch getHigherLevelSwitch(){
		return this.higherLevelSwitch;
	}
	
	protected boolean isHosting(Vm vm){
		return getVmList().contains(vm);
	}
	
	public double getMinPower(){
		return this.getPower(0.0);
	}

	public double getHopNumber(PowerNetHost trg) {
		if(trg==null)
			return 0;
		PowerNetDataCentre dc = (PowerNetDataCentre) getDatacenter();
		double numHops = dc.getNumHops(this, trg);
		return numHops;
	}

	public void computeMigrationEnergy(Vm vm, PowerNetHost source, PowerNetHost target, boolean isSource) {
		this.setMigrationPower( (isSource) ? migModel.getEnergyConsumptionOnSource(vm, source, target):
				migModel.getEnergyConsumptionOnTarget(vm, source, target));		
	}	
	
}
