package it.unisa.cloudsim.powernet;

import org.cloudbus.cloudsim.power.models.PowerModelSpecPower;

public class PowerModelSpecPowerOpteron extends PowerModelSpecPower {

	private final double[] power = { 495.5444154, 681.933093, 735.3100196, 771.5331994, 775.6971012,
			796.377506, 817.0579109, 834.5479013, 863.2943094, 866.4578345, 879.7406711
 };
	
	@Override
	protected double getPowerData(int index) {
		// TODO Auto-generated method stub
		return power[index];
	}

}
