package it.unisa.cloudsim.overcommitment;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cloudbus.cloudsim.DatacenterCharacteristics;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Storage;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.VmAllocationPolicy;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.core.CloudSimTags;
import org.cloudbus.cloudsim.core.SimEvent;
import org.cloudbus.cloudsim.core.predicates.PredicateType;
import org.cloudbus.cloudsim.power.PowerDatacenter;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.provisioners.BwProvisionerSimple;
import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.ext.JGraphModelAdapter;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;

import it.unisa.cloudsim.NAConsolidationConstants;
import it.unisa.cloudsim.powernet.DataCenterSwitch;
import it.unisa.cloudsim.powernet.PowerNetHost;
import it.unisa.cloudsim.powernet.topologies.DCNTopology;
import it.unisa.cloudsim.powernet.topologies.FatTreeDCNTopology;
import it.unisa.cloudsim.powernet.vmmigration.ConstantVMMigrationModel;
import it.unisa.cloudsim.powernet.vmmigration.LiuVMMigrationModel;
import it.unisa.cloudsim.powernet.vmmigration.VMMigrationModel;

public class PowerNetDataCentre extends PowerDatacenter {

	private VMMigrationModel migModel;
	private DCNTopology topology;
	private DataCenterSwitch rootNode;

	public PowerNetDataCentre(String name, DatacenterCharacteristics characteristics,
			VmAllocationPolicy vmAllocationPolicy, List<Storage> storageList, double schedulingInterval)
			throws Exception {
		super(name, characteristics, vmAllocationPolicy, storageList, schedulingInterval);
		this.topology = new FatTreeDCNTopology(DefaultEdge.class, NAConsolidationConstants.SWITCH_CAPACITY);
		this.migModel = NAConsolidationConstants.MIGRATION_ENERGY_MODEL.newInstance();
	}
	
		
	protected void processVmMigrate(SimEvent ev, boolean ack){
			super.processVmMigrate(ev, ack);
			//add energy consumption
			Object tmp = ev.getData();
			if (!(tmp instanceof Map<?, ?>)) {
				throw new ClassCastException("The data object must be Map<String, Object>");
			}

			@SuppressWarnings("unchecked")
			Map<String, Object> migrate = (HashMap<String, Object>) tmp;

			Vm vm = (Vm) migrate.get("vm");
			PowerNetHost source = (PowerNetHost) migrate.get("previousHost"); 
			PowerNetHost target = (PowerNetHost) migrate.get("host");
			source.computeMigrationEnergy(vm, source, target, true);
			target.computeMigrationEnergy(vm, source, target, false);
			//setPower(getPower() + VMMigrationModel.getEnergyConsumption(vm, source, target));
	}

	
	
	public void setupNetworkTopology(List<PowerNetHost> hostList){
		topology.setupTopology(hostList);
	}


	public int getNumHops(PowerNetHost src, PowerNetHost trg) 
													throws RuntimeException{
		if (!NAConsolidationConstants.CONSIDER_HOPS)
			return 1;
		DijkstraShortestPath<PowerMeasurable, DefaultEdge> sp = 
				new DijkstraShortestPath<PowerMeasurable, DefaultEdge>((Graph<PowerMeasurable, DefaultEdge>) topology);
		GraphPath<PowerMeasurable, DefaultEdge>	path =
				sp.getPath(src, trg);
		if(path.getEdgeList() == null)
			throw new RuntimeException("No path between " + src.getId() + " and " + trg.getId());
		if(path.getEdgeList().isEmpty())
			return 0;
		return path.getEdgeList().size();
	}
	
	public DCNTopology getTopology(){
		return topology;
	}

	public PowerMeasurable getRoot() {
		return rootNode;
	}
	
	protected void updateCloudletProcessing() {
		if (getCloudletSubmitted() == -1 || getCloudletSubmitted() == CloudSim.clock()) {
			CloudSim.cancelAll(getId(), new PredicateType(CloudSimTags.VM_DATACENTER_EVENT));
			schedule(getId(), getSchedulingInterval(), CloudSimTags.VM_DATACENTER_EVENT);
			return;
		}
		double currentTime = CloudSim.clock();

		// if some time passed since last processing
		if (currentTime > getLastProcessTime()) {
			System.out.print(currentTime + " ");

			double minTime = updateCloudetProcessingWithoutSchedulingFutureEventsForce();

			if (!isDisableMigrations()) {
				List<Map<String, Object>> migrationMap = getVmAllocationPolicy().optimizeAllocation(
						getVmList());

				if (migrationMap != null) {
					for (Map<String, Object> migrate : migrationMap) {
						Vm vm = (Vm) migrate.get("vm");
						PowerNetHost targetHost = (PowerNetHost) migrate.get("host");
						PowerNetHost oldHost = (PowerNetHost) migrate.get("previousHost");
						
						if (oldHost == null) {
							Log.formatLine(
									"%.2f: Migration of VM #%d to Host #%d is started",
									currentTime,
									vm.getId(),
									targetHost.getId());
						} else {
							//migrate.put("previousHost", oldHost);
							Log.formatLine(
									"%.2f: Migration of VM #%d from Host #%d to Host #%d is started",
									currentTime,
									vm.getId(),
									oldHost.getId(),
									targetHost.getId());
						}

						targetHost.addMigratingInVm(vm);
						incrementMigrationCount();

						/** VM migration delay = RAM / bandwidth **/
						// we use BW / 2 to model BW available for migration purposes, the other
						// half of BW is for VM communication
						// around 16 seconds for 1024 MB using 1 Gbit/s network
						int numHops = getNumHops(oldHost, targetHost);
						send(
								getId(),
								migModel.getTransferTimeForSingleHop(vm.getRam(), oldHost.getBw(), targetHost.getBw()) *
								numHops,
								CloudSimTags.VM_MIGRATE,
								migrate);
					}
				}
			}

			// schedules an event to the next time
			if (minTime != Double.MAX_VALUE) {
				CloudSim.cancelAll(getId(), new PredicateType(CloudSimTags.VM_DATACENTER_EVENT));
				send(getId(), getSchedulingInterval(), CloudSimTags.VM_DATACENTER_EVENT);
			}

			setLastProcessTime(currentTime);
		}
	
}}
