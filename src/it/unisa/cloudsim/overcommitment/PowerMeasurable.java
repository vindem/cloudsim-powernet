package it.unisa.cloudsim.overcommitment;

public interface PowerMeasurable {

	public double getPower();
	
}
