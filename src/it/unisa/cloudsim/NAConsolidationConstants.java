package it.unisa.cloudsim;

import org.cloudbus.cloudsim.power.models.PowerModel;
import org.cloudbus.cloudsim.power.models.PowerModelSpecPowerHpProLiantMl110G4Xeon3040;
import org.cloudbus.cloudsim.power.models.PowerModelSpecPowerHpProLiantMl110G5Xeon3075;

import it.unisa.cloudsim.powernet.PowerModelSpecPowerOpteron;
import it.unisa.cloudsim.powernet.PowerModelSpecPowerXeon;
import it.unisa.cloudsim.powernet.vmmigration.ConstantVMMigrationModel;
import it.unisa.cloudsim.powernet.vmmigration.LiuVMMigrationModel;
import it.unisa.cloudsim.powernet.vmmigration.VMMigrationModel;
import it.unisa.cloudsim.powernet.vmmigration.WAVM3VMMigrationModel;

/**
 * If you are using any algorithms, policies or workload included in the power package please cite
 * the following paper:
 * 
 * Anton Beloglazov, and Rajkumar Buyya, "Optimal Online Deterministic Algorithms and Adaptive
 * Heuristics for Energy and Performance Efficient Dynamic Consolidation of Virtual Machines in
 * Cloud Data Centers", Concurrency and Computation: Practice and Experience (CCPE), Volume 24,
 * Issue 13, Pages: 1397-1420, John Wiley & Sons, Ltd, New York, USA, 2012
 * 
 * @author Vincenzo De Maio
 * @since 14 Mar, 2016
 */
public class NAConsolidationConstants {

	static final int SECONDS_PER_DAY = 24*60*60;
	
	static final int SIMULATION_DAYS = 1;
	
	public static Class<? extends VMMigrationModel> MIGRATION_ENERGY_MODEL = WAVM3VMMigrationModel.class;
	
	public final static int NUMBER_OF_VMS = 96;

	public final static int NUMBER_OF_HOSTS = 800;

	public final static long CLOUDLET_UTILIZATION_SEED = 1;
	
	public final static double SIMULATION_LIMIT = SIMULATION_DAYS * SECONDS_PER_DAY;
	//public final static double SIMULATION_LIMIT = 328;
	
	public static final boolean ENABLE_OUTPUT = false;

	public final static double SCHEDULING_INTERVAL = 40;
	
	public final static int CLOUDLET_LENGTH	= 2500 * (int) SIMULATION_LIMIT;
	public final static int CLOUDLET_PES	= 1;
	
	/*
	 * VM instance types:
	 *   High-Memory Extra Large Instance: 3.25 EC2 Compute Units, 8.55 GB // too much MIPS
	 *   High-CPU Medium Instance: 2.5 EC2 Compute Units, 0.85 GB
	 *   Extra Large Instance: 2 EC2 Compute Units, 3.75 GB
	 *   Small Instance: 1 EC2 Compute Unit, 1.7 GB
	 *   Micro Instance: 0.5 EC2 Compute Unit, 0.633 GB
	 *   We decrease the memory size two times to enable oversubscription
	 *
	 */
	private static final int MBYTES_PER_GB = 1000;
	private static final int BYTES_PER_GB = 1000000;
	public final static int VM_TYPES	= 4;
	public final static int[] VM_MIPS	= { 2500, 2000, 1000, 500 };
	public final static int[] VM_PES	= { 1, 1, 1, 1 };
	public final static int[] VM_RAM	= { (int)8.7*MBYTES_PER_GB,  (int)17.4*MBYTES_PER_GB, (int)17.4*MBYTES_PER_GB, (int)6.13*MBYTES_PER_GB };
	public final static int VM_BW		= 100000; // 100 Mbit/s
	public final static int VM_SIZE		= (int)2.5 * MBYTES_PER_GB; // 2.5 GB

	/*
	 * Host types:
	 *   HP ProLiant ML110 G4 (1 x [Xeon 3040 1860 MHz, 2 cores], 4GB)
	 *   HP ProLiant ML110 G5 (1 x [Xeon 3075 2660 MHz, 2 cores], 4GB)
	 *   We increase the memory size to enable over-subscription (x4)
	 */
	public final static int HOST_TYPES	 = 2;
	public final static int[] HOST_MIPS	 = { 2300, 2900 };
	public final static int[] HOST_PES	 = { 32, 40 };

	
	public final static int[] HOST_RAM	 = { 32*MBYTES_PER_GB, 128*MBYTES_PER_GB };
	public final static int HOST_BW		 = 1000000; // 1 Gbit/s
	public final static int HOST_STORAGE = 6000000; // 6 GB

	public final static PowerModel[] HOST_POWER = {
		new PowerModelSpecPowerOpteron(),
		new PowerModelSpecPowerXeon()
	};
	
	public static final double SWITCH_POWER = 10.0;
	public static final int SWITCH_CAPACITY = 32;

	public static final long SWITCH_BW = 1000000;

	public static final boolean OUTPUT_CSV = false;

	public static final double VM_MIGRATION_PRECOPY_ITERATION = 1;
	public static final double PAGE_DIRTYING_RATE = 0.1;

	public static final double PAGE_SIZE = 4096/10e6;
	public static final boolean CONSIDER_HOPS = true;
}
