package it.unisa.cloudsim;

import java.io.File;
import java.util.Calendar;
import java.util.List;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.examples.power.Constants;
import org.cloudbus.cloudsim.examples.power.Helper;
import org.cloudbus.cloudsim.examples.power.random.RandomConstants;
import org.cloudbus.cloudsim.examples.power.random.RandomHelper;
import org.cloudbus.cloudsim.power.PowerDatacenter;
import org.cloudbus.cloudsim.power.PowerDatacenterNonPowerAware;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerVmAllocationPolicyMigrationAbstract;
import org.cloudbus.cloudsim.power.PowerVmAllocationPolicyMigrationInterQuartileRange;
import org.cloudbus.cloudsim.power.PowerVmAllocationPolicyMigrationLocalRegression;
import org.cloudbus.cloudsim.power.PowerVmAllocationPolicyMigrationLocalRegressionRobust;
import org.cloudbus.cloudsim.power.PowerVmAllocationPolicyMigrationMedianAbsoluteDeviation;
import org.cloudbus.cloudsim.power.PowerVmAllocationPolicyMigrationStaticThreshold;
import org.cloudbus.cloudsim.power.PowerVmAllocationPolicySimple;
import org.cloudbus.cloudsim.power.PowerVmSelectionPolicy;
import org.cloudbus.cloudsim.power.PowerVmSelectionPolicyMaximumCorrelation;
import org.cloudbus.cloudsim.power.PowerVmSelectionPolicyMinimumMigrationTime;
import org.cloudbus.cloudsim.power.PowerVmSelectionPolicyRandomSelection;
import org.jgrapht.ext.MatrixExporter;
import org.jgrapht.graph.DefaultEdge;

import it.unisa.cloudsim.firstfit.FirstFitVMAllocation;
import it.unisa.cloudsim.overcommitment.*;
import it.unisa.cloudsim.powernet.PowerNetHost;
import it.unisa.cloudsim.powernet.topologies.FatTreeDCNTopology;
import it.unisa.cloudsim.powernet.vmmigration.LiuVMMigrationModel;
import it.unisa.cloudsim.powernet.vmmigration.policies.ClosestTargetVMAllocationPolicy;

/**
 * 
 * @author Vincenzo De Maio
 * @since 14 Mar 2016
 */

public class NAConsolidation {

	private static final String WORKLOAD_PATH = System.getProperty("user.dir")+"/bin/workload/planetlab/20110303/";

	public static void main(String[] args) {
		String experimentName = "NA VM Consolidation";
		String outputFolder = "output";

		Log.setDisabled(NAConsolidationConstants.ENABLE_OUTPUT);
		Log.printLine("Starting " + experimentName);

		try {
			CloudSim.init(1, Calendar.getInstance(), false);

			DatacenterBroker broker = Helper.createBroker();
			int brokerId = broker.getId();

			List<Cloudlet> cloudletList = NAConsolidationHelper.createPlanetLabCloudletList(
					brokerId,
					NAConsolidationConstants.NUMBER_OF_VMS,
					WORKLOAD_PATH);
			List<Vm> vmList = Helper.createVmList(brokerId, cloudletList.size());
			List<PowerNetHost> hostList = NAConsolidationHelper.createHostList(NAConsolidationConstants.NUMBER_OF_HOSTS);
			
			
			PowerVmSelectionPolicy vmSelectionPolicy = new PowerVmSelectionPolicyRandomSelection();
			/*
			PowerVmAllocationPolicyMigrationAbstract fallbackVmAllocationPolicy = new PowerVmAllocationPolicyMigrationStaticThreshold(
					hostList,
					vmSelectionPolicy,
					0.7);
			
			PowerVmAllocationPolicyMigrationAbstract vmAllocationPolicy = new PowerVmAllocationPolicyMigrationStaticThreshold(
					hostList,
					vmSelectionPolicy,
					0.8);
			*/
			PowerVmAllocationPolicyMigrationAbstract fallbackVmAllocationPolicy = new ClosestTargetVMAllocationPolicy(
					hostList,
					vmSelectionPolicy);
			
			PowerVmAllocationPolicyMigrationAbstract vmAllocationPolicy = new ClosestTargetVMAllocationPolicy(
					hostList,
					vmSelectionPolicy);
			
			PowerNetDataCentre datacenter =  (PowerNetDataCentre) NAConsolidationHelper.createDatacenter(
					"Datacenter",
					PowerNetDataCentre.class,
					hostList,
					vmAllocationPolicy
					);
			
			datacenter.setDisableMigrations(false);
			datacenter.setupNetworkTopology(hostList);			
			
			broker.submitVmList(vmList);
			broker.submitCloudletList(cloudletList);

			CloudSim.terminateSimulation(NAConsolidationConstants.SIMULATION_LIMIT);
			double lastClock = CloudSim.startSimulation();

			List<Cloudlet> newList = broker.getCloudletReceivedList();
			Log.printLine("Received " + newList.size() + " cloudlets");

			CloudSim.stopSimulation();

			Helper.printResults(
					datacenter,
					vmList,
					lastClock,
					experimentName,
					NAConsolidationConstants.OUTPUT_CSV,
					outputFolder);
			
			System.out.println(NAConsolidationConstants.MIGRATION_ENERGY_MODEL.getCanonicalName());
			System.out.println(datacenter.getMigrationCount());
			System.out.println(datacenter.getPower() / (3600 * 1000));
									
			FatTreeDCNTopology topology = (FatTreeDCNTopology) datacenter.getTopology();
			MatrixExporter<PowerMeasurable, DefaultEdge> exporter = new MatrixExporter();
			File outFile = new File("topology_test");
			//exporter.exportGraph(topology, outFile);
		} catch (Exception e) {
			e.printStackTrace();
			Log.printLine("The simulation has been terminated due to an unexpected error");
			System.exit(0);
		}

		Log.printLine("Finished " + experimentName);
	}

}


