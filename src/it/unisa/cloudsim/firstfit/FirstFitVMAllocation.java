package it.unisa.cloudsim.firstfit;

import java.util.List;
import java.util.Map;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerVmAllocationPolicyAbstract;
import org.cloudbus.cloudsim.power.PowerVmAllocationPolicyMigrationAbstract;
import org.cloudbus.cloudsim.power.PowerVmSelectionPolicy;

public class FirstFitVMAllocation extends PowerVmAllocationPolicyMigrationAbstract {

	public FirstFitVMAllocation(List<? extends Host> list,
			PowerVmSelectionPolicy vmSelectionPolicy) {
		super(list, vmSelectionPolicy);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected boolean isHostOverUtilized(PowerHost host) {
		return host.getAvailableMips() <= 500;
	}

}
