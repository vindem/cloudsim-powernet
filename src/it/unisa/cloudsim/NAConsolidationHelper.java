/*
 *
 */
package it.unisa.cloudsim;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.Datacenter;
import org.cloudbus.cloudsim.DatacenterCharacteristics;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Pe;
import org.cloudbus.cloudsim.Storage;
import org.cloudbus.cloudsim.UtilizationModel;
import org.cloudbus.cloudsim.UtilizationModelFull;
import org.cloudbus.cloudsim.UtilizationModelNull;
import org.cloudbus.cloudsim.UtilizationModelPlanetLabInMemory;
import org.cloudbus.cloudsim.UtilizationModelStochastic;
import org.cloudbus.cloudsim.VmAllocationPolicy;
import org.cloudbus.cloudsim.VmSchedulerTimeSharedOverSubscription;
import org.cloudbus.cloudsim.examples.power.Constants;
import org.cloudbus.cloudsim.examples.power.Helper;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerHostUtilizationHistory;
import org.cloudbus.cloudsim.provisioners.BwProvisionerSimple;
import org.cloudbus.cloudsim.provisioners.PeProvisionerSimple;
import org.cloudbus.cloudsim.provisioners.RamProvisionerSimple;

import it.unisa.cloudsim.firstfit.FirstFitVMAllocation;
import it.unisa.cloudsim.overcommitment.PowerMeasurable;
import it.unisa.cloudsim.overcommitment.PowerNetDataCentre;
import it.unisa.cloudsim.powernet.DataCenterSwitch;
import it.unisa.cloudsim.powernet.PowerNetHost;
import it.unisa.cloudsim.powernet.vmmigration.ConstantVMMigrationModel;
import it.unisa.cloudsim.powernet.vmmigration.LiuVMMigrationModel;
import it.unisa.cloudsim.powernet.vmmigration.VMMigrationModel;
import it.unisa.cloudsim.powernet.vmmigration.WAVM3VMMigrationModel;

/**
 * The Helper class for the random workload.
 * 
 * If you are using any algorithms, policies or workload included in the power package please cite
 * the following paper:
 * 
 * Anton Beloglazov, and Rajkumar Buyya, "Optimal Online Deterministic Algorithms and Adaptive
 * Heuristics for Energy and Performance Efficient Dynamic Consolidation of Virtual Machines in
 * Cloud Data Centers", Concurrency and Computation: Practice and Experience (CCPE), Volume 24,
 * Issue 13, Pages: 1397-1420, John Wiley & Sons, Ltd, New York, USA, 2012
 * 
 * @author Vincenzo De Maio
 * @since Mar 14 , 2016
 */
public class NAConsolidationHelper{

	/**
	 * Creates the cloudlet list.
	 * 
	 * @param brokerId the broker id
	 * @param cloudletsNumber the cloudlets number
	 * 
	 * @return the list< cloudlet>
	 */
	public static List<Cloudlet> createPlanetLabCloudletList(int brokerId, int cloudletsNumber, String path) {
		List<Cloudlet> list = new ArrayList<Cloudlet>();

		long fileSize = 300;
		long outputSize = 300;
		UtilizationModel utilizationModelFull = new UtilizationModelFull();

		File inputFolder = new File(path);
		File[] files = inputFolder.listFiles();

		for (int i = 0; i < files.length; i++) {
			Cloudlet cloudlet = null;
			try {
				cloudlet = new Cloudlet(
						i,
						Constants.CLOUDLET_LENGTH,
						Constants.CLOUDLET_PES,
						fileSize,
						outputSize,
						new UtilizationModelPlanetLabInMemory(
								files[i].getAbsolutePath(),
								Constants.SCHEDULING_INTERVAL), utilizationModelFull, utilizationModelFull);
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(0);
			}
			cloudlet.setUserId(brokerId);
			cloudlet.setVmId(i);
			list.add(cloudlet);
		}

		return list;
	}
	
	/**
	 * Creates the cloudlet list.
	 * 
	 * @param brokerId the broker id
	 * @param cloudletsNumber the cloudlets number
	 * 
	 * @return the list< cloudlet>
	 */
	public static List<Cloudlet> createCloudletList(int brokerId, int cloudletsNumber) {
		List<Cloudlet> list = new ArrayList<Cloudlet>();

		long fileSize = 300;
		long outputSize = 300;
		long seed = NAConsolidationConstants.CLOUDLET_UTILIZATION_SEED;
		UtilizationModel utilizationModelFull = new UtilizationModelFull();
		
		for (int i = 0; i < cloudletsNumber; i++) {
			Cloudlet cloudlet = null;
			if (seed == -1) {
				cloudlet = new Cloudlet(
						i,
						NAConsolidationConstants.CLOUDLET_LENGTH,
						NAConsolidationConstants.CLOUDLET_PES,
						fileSize,
						outputSize,
						//new UtilizationModelStochastic(),
						utilizationModelFull,
						utilizationModelFull,
						utilizationModelFull);
			} else {
				cloudlet = new Cloudlet(
						i,
						NAConsolidationConstants.CLOUDLET_LENGTH,
						NAConsolidationConstants.CLOUDLET_PES,
						fileSize,
						outputSize,
						//new UtilizationModelStochastic(seed * i),
						utilizationModelFull,
						utilizationModelFull,
						utilizationModelFull);
			}
			cloudlet.setUserId(brokerId);
			cloudlet.setVmId(i);
			list.add(cloudlet);
		}

		return list;
	}

	public static List<PowerNetHost> createHostList(int hostsNumber) {
		List<PowerNetHost> hostList = new ArrayList<PowerNetHost>();
		
		for (int i = 0; i < hostsNumber; i++) {
			
			int hostType = i % Constants.HOST_TYPES;

			List<Pe> peList = new ArrayList<Pe>();
			for (int j = 0; j < Constants.HOST_PES[hostType]; j++) {
				peList.add(new Pe(j, new PeProvisionerSimple(Constants.HOST_MIPS[hostType])));
			}
			
			
				hostList.add(new PowerNetHost(
						i,
						new RamProvisionerSimple(Constants.HOST_RAM[hostType]),
						new BwProvisionerSimple(Constants.HOST_BW),
						Constants.HOST_STORAGE,
						peList,
						new VmSchedulerTimeSharedOverSubscription(peList),
						Constants.HOST_POWER[hostType],
						NAConsolidationConstants.MIGRATION_ENERGY_MODEL)
						);
			
			}
		
		return hostList;
	}
	
	public static PowerNetDataCentre createDatacenter(
			String name,
			Class<? extends Datacenter> datacenterClass,
			List<? extends Host> hostList,
			VmAllocationPolicy vmAllocationPolicy) throws Exception 
	{
		String arch = "x86"; // system architecture
		String os = "Linux"; // operating system
		String vmm = "Xen";
		double time_zone = 10.0; // time zone this resource located
		double cost = 3.0; // the cost of using processing in this resource
		double costPerMem = 0.05; // the cost of using memory in this resource
		double costPerStorage = 0.001; // the cost of using storage in this resource
		double costPerBw = 0.0; // the cost of using bw in this resource

		DatacenterCharacteristics characteristics = new DatacenterCharacteristics(
				arch,
				os,
				vmm,
				hostList,
				time_zone,
				cost,
				costPerMem,
				costPerStorage,
				costPerBw);

		PowerNetDataCentre datacenter = null;
		try {
			datacenter = (PowerNetDataCentre) datacenterClass.getConstructor(
					String.class,
					DatacenterCharacteristics.class,
					VmAllocationPolicy.class,
					List.class,
					Double.TYPE
					).newInstance(
					name,
					characteristics,
					vmAllocationPolicy,
					new LinkedList<Storage>(),
					Constants.SCHEDULING_INTERVAL);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
		//datacenter.setupNetworkTopology(hostList);
		return (PowerNetDataCentre) datacenter;
		
		
	}
}
